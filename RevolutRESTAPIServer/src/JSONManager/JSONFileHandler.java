package JSONManager;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Handler for reading and writing to JSON files.
 * 
 */
public class JSONFileHandler {
    private final JSONParser parser = new JSONParser();
    
    
    /**
     * Reads JSON file and returns JSON object.
     * 
     * @param filePath - path to JSON file
     * @return whole JSON file as JSON object
     * @throws ParseException
     * @throws IOException
     */
    public synchronized JSONObject readJSONObject(String filePath)
            throws ParseException, IOException {
        FileReader jsonFile = new FileReader(filePath);
        JSONObject jsonObject = (JSONObject)parser.parse(jsonFile);
        jsonFile.close();
        return jsonObject;
    }
    
    
    /**
     * Writes to JSON file.
     * 
     * @param filePath - path to JSON file
     * @param jsonObject - JSON object to be saved in file
     * @throws IOException
     */
    public synchronized void writeJSONObject(String filePath,
                                             JSONObject jsonObject)
                                                     throws IOException {
        FileWriter jsonFile = new FileWriter(filePath);
        jsonFile.write(jsonObject.toJSONString());
        jsonFile.flush();
        jsonFile.close();
    }
}