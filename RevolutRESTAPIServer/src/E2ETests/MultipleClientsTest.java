package E2ETests;

import static org.junit.jupiter.api.Assertions.*;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.net.URL;

import java.util.stream.Collectors;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import sun.net.www.protocol.http.HttpURLConnection;

import org.json.simple.JSONObject;

import JSONManager.JSONFileHandler;

/**
 * Multiple Clients Test simulates requests sent by multiple clients.
 *
 */
public class MultipleClientsTest {
    private final String accountsFilePath = "db/accounts.json";
    private final JSONFileHandler jsonFileHandler =
            new JSONFileHandler();
    private final String accountNumber1 = "9001";
    private final String accountNumber2 = "9002";
    private final String initialBalance1 = "1000.0";
    private final String initialBalance2 = "1000.0";
    
    /**
     * Sends HTTP request and returns response.
     * 
     * @param method - request method
     * @param request - request URL String
     * @return response
     */
    private String sendHTTPRequest(String method, String request) {
        String response = null;
        HttpURLConnection connection = null;
        
        try {
            URL url = new URL(request);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(method);
            System.out.println(connection);
            connection.setRequestProperty("Content-Type", 
                "application/x-www-form-urlencoded");
    
            connection.setRequestProperty("Content-Language", "en-US");
            connection.setUseCaches(false);
            
            InputStream input = connection.getInputStream();
            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(input));
            response = reader
                .lines()
                .collect(Collectors.joining());
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        
        return response;
    }
    
    
    /**
     * Initializes 2 accounts for transfering.
     */
    @BeforeEach
    private void init() {
        sendHTTPRequest(
            "POST",
            (new StringBuilder()
                    .append("http://localhost:8080/account?number=")
                    .append(accountNumber1)
                    .append("&first_name=A")
                    .append("&last_name=B")
                    .append("&balance=")
                    .append(initialBalance1)
                    .toString())
        );
        sendHTTPRequest(
            "POST",
            (new StringBuilder()
                    .append("http://localhost:8080/account?number=")
                    .append(accountNumber2)
                    .append("&first_name=A")
                    .append("&last_name=B")
                    .append("&balance=")
                    .append(initialBalance2)
                    .toString())
        );
    }
    
    
    /**
     * Cleans up test accounts.
     */
    @AfterEach
    private void clean() {
        sendHTTPRequest(
            "DELETE",
            (new StringBuilder()
                    .append("http://localhost:8080/account?number=")
                    .append(accountNumber1)
                    .toString())
        );
        sendHTTPRequest(
            "DELETE",
            (new StringBuilder()
                    .append("http://localhost:8080/account?number=")
                    .append(accountNumber2)
                    .toString())
        );
    }
    
    
    /**
     * Sends HTTP transfer requests given number times
     * from sender to receiver.
     * 
     * @param times
     */
    private void transfer(String sender,
                          String receiver,
                          String amount,
                          int times) {
        for (int i = 0; i < times; ++i) {
            sendHTTPRequest(
                "PUT",
                (new StringBuilder()
                        .append("http://localhost:8080/transfer?sender=")
                        .append(sender)
                        .append("&receiver=")
                        .append(receiver)
                        .append("&amount=")
                        .append(amount)
                        .toString())
            );            
        }
    }
    

    /**
     * Transfers transferAmount between accounts.
     * Transfers are done independently.
     * After that, checks accounts balances.
     * 
     * @param timeout - timeout for client threads
     * @param transferAmount - transfer amount
     * @param times1to2 - amount transfers from account 1 to 2
     * @param times2to1 - amount transfers from account 2 to 1
     * @param expectedBalance1 - expected balance on account 1
     * @param expectedBalance2 - expected balance on account 2
     */
    @ParameterizedTest
    @CsvSource({
        "10000, 1.0, 100, 50, 950.0, 1050.0",
        "10000, 1.0, 123, 23, 900.0, 1100.0"
    })
    public void testTransfer(int timeout,
                     String transferAmount,
                     int times1to2,
                     int times2to1,
                     String expectedBalance1,
                     String expectedBalance2) {
        Runnable client1 = () -> { transfer(accountNumber1,
                                            accountNumber2,
                                            transferAmount,
                                            times1to2); };

        Runnable client2 = () -> { transfer(accountNumber2,
                                            accountNumber1,
                                            transferAmount,
                                            times2to1); };
        
        Thread client1Thread = new Thread(client1);        
        Thread client2Thread = new Thread(client2);
        
        client1Thread.start();        
        client2Thread.start();
        
        try {
            client1Thread.join(timeout);
            client2Thread.join(timeout);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
        try {
            JSONObject jsonObject =
                    jsonFileHandler.readJSONObject(accountsFilePath);
            System.out.println(jsonObject);
            JSONObject account1 = (JSONObject) jsonObject.get(accountNumber1);
            JSONObject account2 = (JSONObject) jsonObject.get(accountNumber2);
            
            assertEquals(expectedBalance1, account1.get("balance").toString());
            assertEquals(expectedBalance2, account2.get("balance").toString());
        } catch (Exception e) {
            e.printStackTrace();
            fail(e);
        }
    }
}
