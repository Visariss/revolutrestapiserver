package AccountsManager;
import java.io.IOException;
import java.util.TreeMap;
import java.math.BigDecimal;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import JSONManager.JSONFileHandler;

/**
 * Handler for accounts. Contains methods to modify accounts data store.
 * 
 */
public class AccountsHandler {
    private static final JSONFileHandler jsonFileHandler =
            new JSONFileHandler();
    private static final String success = "OK";
    
    private String accountsFilePath;

    
    /**
     * Should be thrown if account with given number does not exist.
     * 
     */
    public class AccountNotFoundException extends Exception {
        private static final long serialVersionUID = 4077175853228115303L;

        public AccountNotFoundException() {
            super("ERROR: Account with the given number does not exist.\r\n");
        }
    }

    
    /**
     * Should be thrown if account with given number
     * already exists and cannot be added.
     * 
     */
    public class AccountNotUniqueException extends Exception {
        private static final long serialVersionUID = 4029890541614513574L;

        public AccountNotUniqueException() {
            super("ERROR: Account with the given number already exists.\r\n");
        }
    }

    
    /**
     * Should be thrown if account has insufficient balance
     * to make a transfer with requested amount.
     * 
     */
    public class InsufficientBalanceException extends Exception {
        private static final long serialVersionUID = 3724266377213400219L;

        public InsufficientBalanceException() {
            super("ERROR: Account has insufficient balance.\r\n");
        }
    }
    
    
    /**
     * Constructor
     * 
     * @param accountsFilePath - file path to accounts data store.
     */
    public AccountsHandler(String accountsFilePath) {
        this.accountsFilePath = accountsFilePath;
    }
    
    
    /**
     * Checks if account exists and returns true if it does.
     * 
     * @param accountNumber - account number
     * @return true if account exists, false otherwise
     */
    public synchronized boolean accountExists(String accountNumber) {
        try {
            JSONObject jsonObject =
                    jsonFileHandler.readJSONObject(accountsFilePath);
            if (jsonObject.get(accountNumber) != null)
                return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    

    /**
     * Gets account with given number or all accounts
     * if account number is null.
     * Returns response for client with result.
     * If success, response is JSON String.
     * 
     * @param accountNumber - account number
     * @return response
     */
    public synchronized String getAccount(String accountNumber) {
        String response;
        
        try {
            JSONObject jsonObject =
                    jsonFileHandler.readJSONObject(accountsFilePath);
            
            if (jsonObject.get(accountNumber) == null)
                throw new AccountNotFoundException();
            response = jsonObject.get(accountNumber).toString();
        } catch (ParseException e) {
            e.printStackTrace();
            response = "ERROR: Parsing JSON file failed.\r\n";
        } catch (IOException e) {
            e.printStackTrace();
            response = "ERROR: JSON file not found failed.\r\n";
        } catch (Exception e) {
            e.printStackTrace();
            response = e.getMessage();
        }
        
        return response;
    }
    
    
    /**
     * Gets all accounts from accounts data store.
     * Returns response for client with result.
     * If result is success, response is JSON String.
     * 
     * @return response
     */
    public synchronized String getAccounts() {
        String response;
        
        try {            
            JSONObject jsonObject = jsonFileHandler
                    .readJSONObject(accountsFilePath);
            response = jsonObject.toString();
        } catch (ParseException e) {
            e.printStackTrace();
            response = "ERROR: Parsing JSON file failed.\r\n";
        } catch (IOException e) {
            e.printStackTrace();
            response = "ERROR: JSON file not found failed.\r\n";
        } catch (Exception e) {
            e.printStackTrace();
            response = e.getMessage();
        }
        
        return response;
    }
    
    
    /**
     * Edits existed field in account or adds new one.
     * Returns response for client with result.
     * 
     * @param accountNumber - account number
     * @param key - field name
     * @param value - new value
     * @return response
     */
    public synchronized String editAccount(String accountNumber,
                                           String key,
                                           String value) {
        String response;
        
        try {
            JSONObject jsonObject = jsonFileHandler
                    .readJSONObject(accountsFilePath);
            JSONObject account = (JSONObject)(jsonObject.get(accountNumber));
            
            // Generates unsolvable warning - JSONObject class, that extends
            // HashMap does not have type parameter <K,V> in class definition.
            account.put(key, value);

            jsonFileHandler.writeJSONObject(accountsFilePath, jsonObject);
            response = success;
        } catch (ParseException e) {
            e.printStackTrace();
            response = "ERROR: Parsing JSON file failed.\r\n";
        } catch (IOException e) {
            e.printStackTrace();
            response = "ERROR: JSON file not found failed.\r\n";
        } catch (Exception e) {
            e.printStackTrace();
            response = e.getMessage();
        }
        
        return response;        
    }
    
    
    /**
     * Transfers given amount between accounts.
     * Transfers amount has to be equals or greater
     * than sender account balance.
     * Returns response for client with result.
     * 
     * @param senderAccountNumber - sender account number
     * @param receiverAccountNumber - receiver account number
     * @param transferAmount - transfer amount
     * @return response
     */
    public synchronized String transfer(String senderAccountNumber,
                                        String receiverAccountNumber,
                                        String transferAmount) {
        String response;
        BigDecimal transfer = new BigDecimal(transferAmount);
        
        try {
            JSONObject jsonObject = jsonFileHandler
                    .readJSONObject(accountsFilePath);            
            JSONObject senderAccount =
                    (JSONObject)(jsonObject.get(senderAccountNumber));
            BigDecimal senderBalance =
                    new BigDecimal(senderAccount.get("balance").toString());
            
            if (senderBalance.compareTo(transfer) == -1)
                throw new InsufficientBalanceException();

            JSONObject receiverAccount =
                    (JSONObject)(jsonObject.get(receiverAccountNumber));
            BigDecimal receiverBalance =
                    new BigDecimal(receiverAccount.get("balance").toString());
            
            senderBalance = senderBalance.subtract(transfer);
            receiverBalance = receiverBalance.add(transfer);
            
            // Generates unsolvable warning - JSONObject class, that extends
            // HashMap does not have type parameter <K,V> in class definition.
            senderAccount.put("balance", senderBalance);
            receiverAccount.put("balance", receiverBalance);

            jsonFileHandler.writeJSONObject(accountsFilePath, jsonObject);
            response = success;
        } catch (ParseException e) {
            e.printStackTrace();
            response = "ERROR: Parsing JSON file failed.\r\n";
        } catch (IOException e) {
            e.printStackTrace();
            response = "ERROR: JSON file not found failed.\r\n";
        } catch (Exception e) {
            e.printStackTrace();
            response = e.getMessage();
        }
        
        return response;
    }
    
    
    /**
     * Adds account to accounts data store.
     * Returns response for client with result.
     * 
     * @param accountNumber - account number
     * @param firstName - owner's first name
     * @param lastName - owner's last name
     * @param balance - balance
     * @return response
     */
    public synchronized String addAccount(String accountNumber,
                                          String firstName,
                                          String lastName,
                                          String balance) {
        String response;
        
        try {
            JSONObject jsonObject =
                    jsonFileHandler.readJSONObject(accountsFilePath);
            
            if (jsonObject.get(accountNumber) != null)
                throw new AccountNotUniqueException();
            
            TreeMap<String,String> account = new TreeMap<>();
            account.put("first_name", firstName);
            account.put("last_name", lastName);
            account.put("balance", balance);
            
            // Generates unsolvable warning - JSONObject class, that extends
            // HashMap does not have type parameter <K,V> in class definition.
            jsonObject.put(accountNumber, account);

            jsonFileHandler.writeJSONObject(accountsFilePath, jsonObject);
            response = success;
        } catch (ParseException e) {
            e.printStackTrace();
            response = "ERROR: Parsing JSON file failed.\r\n";
        } catch (IOException e) {
            e.printStackTrace();
            response = "ERROR: JSON file not found failed.\r\n";
        } catch (Exception e) {
            e.printStackTrace();
            response = e.getMessage();
        }

        return response;
    }
    
    
    /**
     * Deletes existed account with given number.
     * Returns response for client with result.
     * 
     * @param accountNumber - account number
     * @return - response
     */
    public synchronized String deleteAccount(String accountNumber) {
        String response;
        
        try {
            JSONObject jsonObject = jsonFileHandler
                    .readJSONObject(accountsFilePath);
            
            if (jsonObject.get(accountNumber) == null)
                throw new AccountNotFoundException();
            
            jsonObject.remove(accountNumber);
            jsonFileHandler.writeJSONObject(accountsFilePath, jsonObject);
            response = success;
        } catch (ParseException e) {
            e.printStackTrace();
            response = "ERROR: Parsing JSON file failed.\r\n";
        } catch (IOException e) {
            e.printStackTrace();
            response = "ERROR: JSON file not found failed.\r\n";
        } catch (Exception e) {
            e.printStackTrace();
            response = e.getMessage();
        }

        return response;
    }
}