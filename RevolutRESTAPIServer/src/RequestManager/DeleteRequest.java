package RequestManager;

import java.util.Map;

/**
 * Represents DELETE request.
 * 
 */
public class DeleteRequest extends Request {

    /**
     * Constructor.
     * 
     * @param context - request context
     * @param params - request parameters
     */
    public DeleteRequest(String context, Map<String, String> params) {
        super(context, params);
    }
    
    
    /* (non-Javadoc)
     * @see RequestManager.Request#executeRequest()
     */
    @Override
    public String executeRequest() throws UnsupportedContextException {
        String response;
        
        switch(context) {
            case "account":
                String accountNumber = params.get("number");
                response = accountsHandler.deleteAccount(accountNumber);
                break;
            default:
                throw new UnsupportedContextException();
        }

        return response;
    }
}