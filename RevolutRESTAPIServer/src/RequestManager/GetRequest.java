package RequestManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Represents GET request.
 * 
 */
public class GetRequest extends Request {

    /**
     * Constructor.
     * 
     * @param context - request context
     * @param params - request parameters
     */
    public GetRequest(String context, Map<String, String> params) {
        super(context, params);
    }
    
    
    /**
     * Returns doc from doc file path.
     * 
     * @return doc String
     */
    private String getDoc() {
        String response;
        try {
            File docFile = new File(docFilePath);
            FileReader fileReader = new FileReader(docFile);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            
            response = bufferedReader
                .lines()
                .collect(Collectors.joining(System.getProperty("line.separator")));
    
            bufferedReader.close();
        } catch (Exception e) {
            e.printStackTrace();
            response = e.getMessage();
        }
        return response;
    }
    
    
    /* (non-Javadoc)
     * @see RequestManager.Request#executeRequest()
     */
    @Override
    public String executeRequest() throws UnsupportedContextException {
        String response;
        
        switch(context) {
            case "doc":
                response = getDoc();
                break;
            case "account":
                String accountNumber = params.get("number");
                response = accountsHandler.getAccount(accountNumber);
                break;
            case "accounts":
                response = accountsHandler.getAccounts();
                break;
            default:
                throw new UnsupportedContextException();
        }
        
        return response;
    }
}