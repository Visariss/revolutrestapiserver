package RequestManager;

import java.util.Map;

import AccountsManager.AccountsHandler;

/**
 * Represents request. To create request object,
 * use one of the class that extends this one.
 *
 */
public abstract class Request {
    protected static final String accountsFilePath = "db/accounts.json";
    protected static final String docFilePath = "config/doc";
    protected static final AccountsHandler accountsHandler =
            new AccountsHandler(accountsFilePath);

    protected Map<String, String> params;
    protected String context;

    
    /**
     * Should be thrown if request context is not supported.
     *
     */
    public class UnsupportedContextException extends Exception {
        private static final long serialVersionUID = 6812640761158978554L;

        public UnsupportedContextException() {
            super("ERROR: Unsupported request context.\r\n");
        }
    }
    
    
    /**
     * Constructor that is called by extended classes.
     * 
     * @param context - request context
     * @param params - request parameters
     */
    public Request(String context, Map<String, String> params) {
        this.context = context;
        this.params = params;
    }
    
    
    /**
     * Executes request. This method should set response for client.
     */
    public abstract String executeRequest() throws UnsupportedContextException;
}