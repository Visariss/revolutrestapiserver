package RequestManager;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Represents POST request.
 *
 */
public class PostRequest extends Request {
    
    /**
     * Constructor.
     * 
     * @param context - request context
     * @param params - request parameters
     */
    public PostRequest(String context, Map<String, String> params) {
        super(context, params);
    }
    
    
    /* (non-Javadoc)
     * @see RequestManager.Request#executeRequest()
     */
    @Override
    public String executeRequest() throws UnsupportedContextException {
        String response;
        
        try {
            switch(context) {
                case "account":
                    String accountNumber = params.get("number");
                    String firstName = params.get("first_name");
                    String lastName = params.get("last_name");
                    String balance =
                            new BigDecimal(params.get("balance")).toString();
                    response = accountsHandler.addAccount(accountNumber,
                                                          firstName,
                                                          lastName,
                                                          balance);
                    break;
                default:
                    throw new UnsupportedContextException();
            }         
        } catch (NumberFormatException e) {
            e.printStackTrace();
            response = "ERROR: Invalid balance.\r\n";
        }
        
        return response;
    }
}