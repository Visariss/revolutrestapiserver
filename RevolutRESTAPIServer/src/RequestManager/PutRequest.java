package RequestManager;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Represents PUT request.
 *
 */
public class PutRequest extends Request {

    /**
     * Constructor.
     * 
     * @param context - request context
     * @param params - request parameters
     */
    public PutRequest(String context, Map<String, String> params) {
        super(context, params);
    }
    
    
    /* (non-Javadoc)
     * @see RequestManager.Request#executeRequest()
     */
    @Override
    public String executeRequest() throws UnsupportedContextException {
        String response;
        
        try {
            switch(context) {
                case "account":
                    String accountNumber = params.get("number");
                    String key = params.get("property");
                    String value = params.get("value");
                    if (key.equals("balance")) new BigDecimal(value);
                    response = accountsHandler.editAccount(accountNumber,
                                                           key,
                                                           value);
                    break;
                case "transfer":
                    String senderAccountNumber = params.get("sender");
                    String receiverAccountNumber = params.get("receiver");
                    String transferAmount =
                            new BigDecimal(params.get("amount")).toString();
                    response = accountsHandler.transfer(senderAccountNumber,
                                                        receiverAccountNumber,
                                                        transferAmount
                                                            .toString());
                    break;
                default:
                    throw new UnsupportedContextException();
            }            
        } catch (NumberFormatException e) {
            e.printStackTrace();
            response = "ERROR: Invalid balance.\r\n";
        }
        
        return response;
    }
}