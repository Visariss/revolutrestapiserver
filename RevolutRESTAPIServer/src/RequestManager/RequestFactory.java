package RequestManager;

import java.util.Map;

/**
 * Represents Request factory that creates instances
 * of classes that extends Request.
 *
 */
public class RequestFactory {    
    /**
     * Should be thrown if request type is not correct or unsupported.
     *
     */
    private class RequestException extends Exception {
        private static final long serialVersionUID = 3918956223776276145L;

        public RequestException() {
            super("ERROR: Invalid request type.\r\n");
        }
    }
    
    
    /**
     * Creates and returns request instance basing on given parameters.
     * Throws corresponding exception if request
     * given by user is not supported.
     * 
     * @param method - request method
     * @param context - request context 
     * @param params - request parameters
     * @return
     * @throws RequestException
     */
    public Request create(String method,
                          String context,
                          Map<String, String> params)
                                  throws RequestException {
        Request request;
        switch(method) {
            case "GET":
                request = new GetRequest(context, params);
                break;
            case "PUT":
                request = new PutRequest(context, params);
                break;
            case "POST":
                request = new PostRequest(context, params);
                break;
            case "DELETE":
                request = new DeleteRequest(context, params);
                break;
            default:
                throw new RequestException();
        }
        
        return request;
    }
}
