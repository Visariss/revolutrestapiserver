package UnitTests;

/**
 * Represents predefined test accounts used in tests.
 *
 */
public final class TestAccounts {
	public class TestAccount1 {
	    public static final String number = "9999";
	    public static final String firstName = "John";
	    public static final String lastName = "Smith";
	    public static final String balance = "9999.99";		
	}
	
	public class TestAccount2 {
	    public static final String number = "8888";
	    public static final String firstName = "Tomasz";
	    public static final String lastName = "Nowak";
	    public static final String balance = "8888.88";		
	}
}
