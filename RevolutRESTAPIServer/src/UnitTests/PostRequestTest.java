package UnitTests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.api.AfterEach;

import org.json.simple.JSONObject;

import AccountsManager.AccountsHandler;
import JSONManager.JSONFileHandler;
import RequestManager.PostRequest;
import RequestManager.Request;

/**
 * Contains tests for PostRequest class.
 *
 */
public class PostRequestTest {
    private final String accountsFilePath = "db/accounts.json";
    private final AccountsHandler accountsHandler =
            new AccountsHandler(accountsFilePath);
    private final JSONFileHandler jsonFileHandler =
            new JSONFileHandler();
    
    /**
     * Deletes predefined test account after each test
     * if it is still present in accounts data store.
     */
    @AfterEach
    public void cleanUp() {
        if (accountsHandler.accountExists(TestAccounts.TestAccount1.number))
            accountsHandler.deleteAccount(TestAccounts.TestAccount1.number);
        
        if (accountsHandler.accountExists(TestAccounts.TestAccount2.number))
            accountsHandler.deleteAccount(TestAccounts.TestAccount2.number);
    }
    

    /**
     * Tests executeRequestTest method.
     * 
     * @param context - request context
     */
    @ParameterizedTest
    @CsvSource({"unknown"})
    public void executeRequestTest1(String context) {        
        try {
            Request request = new PostRequest(context, null);
            assertThrows(Request.UnsupportedContextException.class,
                    () -> request.executeRequest());
        } catch (Exception e) {
            e.printStackTrace();
            fail(e);
        }
    }
    

    /**
     * Tests executeRequestTest method.
     * 
     * @param context - request context
     * @param accountNumber - account number
     * @param firstName - first_name property value
     * @param lastName - first_name property value
     * @param balance - balance property value
     * @param expectedResponse - expected response
     */
    @ParameterizedTest
    @CsvSource({"account, 9999, John, Smith, 9999.99, OK"})
    public void executeRequestTest2(String context,
                                    String accountNumber,
                                    String firstName,
                                    String lastName,
                                    String balance,
                                    String expectedResponse) {
        try {
            Map<String, String> params = new HashMap<String, String>();            
            params.put("number", accountNumber);           
            params.put("first_name", firstName);           
            params.put("last_name", lastName);           
            params.put("balance", balance);
            Request request = new PostRequest(context, params);
            String response = request.executeRequest();
            JSONObject jsonObject = jsonFileHandler
                    .readJSONObject(accountsFilePath);
            JSONObject account = (JSONObject)jsonObject.get(TestAccounts.TestAccount1.number);
            assertEquals(TestAccounts.TestAccount1.firstName, account.get("first_name"));
            assertEquals(TestAccounts.TestAccount1.lastName, account.get("last_name"));
            assertEquals(TestAccounts.TestAccount1.balance, account.get("balance"));
            assertEquals(response, expectedResponse);
        } catch (Exception e) {
            e.printStackTrace();
            fail(e);
        }
    }
}
