package UnitTests;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import ServerManager.Server;

/**
 * Contains tests for Server class.
 *
 */
public class ServerTest {

    /**
     * Tests main method.
     */
    /**
     * @param port - server port
     * @param exitCmd - exit command
     */
    @ParameterizedTest
    @CsvSource({
        "8080, exit",
        "8081, exit",
        "8082, exit"
    })
    public void mainTest(String port, String exitCmd) {
        InputStream exitInput;
        try {
            exitInput = new ByteArrayInputStream(exitCmd.getBytes("UTF-8"));
            System.setIn(exitInput);
            Server.main(new String[] {port});
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            fail(e);
        }
    }
}
