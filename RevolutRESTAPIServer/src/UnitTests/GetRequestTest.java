package UnitTests;

import static org.junit.jupiter.api.Assertions.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;

import org.json.simple.JSONObject;

import AccountsManager.AccountsHandler;
import JSONManager.JSONFileHandler;
import RequestManager.GetRequest;
import RequestManager.Request;

/**
 * Contains tests for GetRequest class.
 *
 */
public class GetRequestTest {
    private final String accountsFilePath = "db/accounts.json";
    private final String docFilePath = "config/doc";
    private final AccountsHandler accountsHandler =
            new AccountsHandler(accountsFilePath);
    private final JSONFileHandler jsonFileHandler =
            new JSONFileHandler();
    
    /**
     * Adds by predefined test account before each test.
     */
    @BeforeEach
    public void initialize() {
        accountsHandler.addAccount(TestAccounts.TestAccount1.number,
                                   TestAccounts.TestAccount1.firstName,
                                   TestAccounts.TestAccount1.lastName,
                                   TestAccounts.TestAccount1.balance);
        
        accountsHandler.addAccount(TestAccounts.TestAccount2.number,
					               TestAccounts.TestAccount2.firstName,
					               TestAccounts.TestAccount2.lastName,
					               TestAccounts.TestAccount2.balance);
    }
    
    /**
     * Deletes predefined test account after each test
     * if it is still present in accounts data store.
     */
    @AfterEach
    public void cleanUp() {
        if (accountsHandler.accountExists(TestAccounts.TestAccount1.number))
            accountsHandler.deleteAccount(TestAccounts.TestAccount1.number);
        
        if (accountsHandler.accountExists(TestAccounts.TestAccount2.number))
            accountsHandler.deleteAccount(TestAccounts.TestAccount2.number);
    }
    
    
    /**
     * Tests executeRequestTest method.
     * 
     * @param context - request context
     */
    @ParameterizedTest
    @CsvSource({"unknown"})
    public void executeRequestTest1(String context) {
        try {           
            Request request = new GetRequest(context, null);
            assertThrows(Request.UnsupportedContextException.class,
                         () -> request.executeRequest());
        } catch (Exception e) {
            e.printStackTrace();
            fail(e);
        }
    }
    

    /**
     * Tests executeRequestTest method.
     * 
     * @param context - request context
     * @param accountNumber - account number
     */
    @ParameterizedTest
    @CsvSource({"account, 9999"})
    public void executeRequestTest2(String context, String accountNumber) {
        try {
            Map<String, String> params = new HashMap<String, String>();            
            params.put("number", accountNumber);
            
            Request request = new GetRequest(context, params);
            String response = request.executeRequest();
            JSONObject jsonObject = jsonFileHandler
                    .readJSONObject(accountsFilePath);
            JSONObject account = (JSONObject)jsonObject.get(TestAccounts.TestAccount1.number);
            
            assertEquals(account.toString(), response);
        } catch (Exception e) {
            e.printStackTrace();
            fail(e);
        }
    }
    

    /**
     * Tests executeRequestTest method.
     * 
     * @param context - request context
     */
    @ParameterizedTest
    @CsvSource({
        "accounts"
    })
    public void executeRequestTest3(String context) {        
        try {            
            Request request = new GetRequest(context, null);
            String response = request.executeRequest();
            JSONObject jsonObject = jsonFileHandler
                    .readJSONObject(accountsFilePath);
            assertEquals(jsonObject.toString(), response);
        } catch (Exception e) {
            e.printStackTrace();
            fail(e);
        }
    }
    

    /**
     * Tests executeRequestTest method.
     * 
     * @param context - request context
     */
    @ParameterizedTest
    @CsvSource({
        "doc"
    })
    public void executeRequestTest4(String context) {
        String expectedResponse;
        
        try {
            Request request = new GetRequest(context, null);
            try {
                File docFile = new File(docFilePath);
                FileReader fileReader = new FileReader(docFile);
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                
                expectedResponse = bufferedReader
                    .lines()
                    .collect(Collectors.joining(System.getProperty("line.separator")));
        
                bufferedReader.close();
            } catch (Exception e) {
                e.printStackTrace();
                expectedResponse = e.getMessage();
            }
            
            String response = request.executeRequest();
            assertEquals(expectedResponse, response);
        } catch (Exception e) {
            e.printStackTrace();
            fail(e);
        }
    }
}
