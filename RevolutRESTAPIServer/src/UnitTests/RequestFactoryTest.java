package UnitTests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import RequestManager.Request;
import RequestManager.RequestFactory;

/**
 * Contains tests for RequestFactory class.
 *
 */
public class RequestFactoryTest {
    /**
     * Tests create method.
     * 
     * @param method - request method
     * @param context - request context
     * @param expected - expected request class
     */
    @ParameterizedTest
    @CsvSource({
        "GET, any, RequestManager.GetRequest",
        "PUT, any, RequestManager.PutRequest",
        "POST, any, RequestManager.PostRequest",
        "DELETE, any, RequestManager.DeleteRequest"
    })
    public void createTest(String method, String context, String expected) {
        RequestFactory requestFactory = new RequestFactory();
        Class<?> expectedCls;
        try {
            expectedCls = Class.forName(expected);
            Request request = requestFactory.create(method,
                                                    context,
                                                    null);
            assertEquals(expectedCls, request.getClass());
        } catch (Exception e) {
            e.printStackTrace();
            fail(e);
        }
    }
}
