package UnitTests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;

import org.json.simple.JSONObject;

import AccountsManager.AccountsHandler;
import JSONManager.JSONFileHandler;
import RequestManager.PutRequest;
import RequestManager.Request;

/**
 * Contains tests for PutRequest class.
 *
 */
public class PutRequestTest {
    private final String accountsFilePath = "db/accounts.json";
    private final AccountsHandler accountsHandler =
            new AccountsHandler(accountsFilePath);
    private final JSONFileHandler jsonFileHandler =
            new JSONFileHandler();
    
    /**
     * Adds by predefined test account before each test.
     */
    @BeforeEach
    public void initialize() {
        accountsHandler.addAccount(TestAccounts.TestAccount1.number,
                                   TestAccounts.TestAccount1.firstName,
                                   TestAccounts.TestAccount1.lastName,
                                   TestAccounts.TestAccount1.balance);
        
        accountsHandler.addAccount(TestAccounts.TestAccount2.number,
					               TestAccounts.TestAccount2.firstName,
					               TestAccounts.TestAccount2.lastName,
					               TestAccounts.TestAccount2.balance);
    }
    
    /**
     * Deletes predefined test account after each test
     * if it is still present in accounts data store.
     */
    @AfterEach
    public void cleanUp() {
        if (accountsHandler.accountExists(TestAccounts.TestAccount1.number))
            accountsHandler.deleteAccount(TestAccounts.TestAccount1.number);
        
        if (accountsHandler.accountExists(TestAccounts.TestAccount2.number))
            accountsHandler.deleteAccount(TestAccounts.TestAccount2.number);
    }
    

    /**
     * Tests executeRequest method.
     * 
     * @param context - request context
     */
    @ParameterizedTest
    @CsvSource({
    	"/account",
    	"/transfer"
    })
    public void executeRequestTest1(String context) {        
        try {
            Request request = new PutRequest(context, null);
            assertThrows(Request.UnsupportedContextException.class,
                    () -> request.executeRequest());
        } catch (Exception e) {
            e.printStackTrace();
            fail(e);
        }
    }
    

    /**
     * Tests executeRequest method.
     * 
     * @param context - request context
     * @param accountNumber - account number
     * @param property - property name
     * @param value - property value
     * @param expected1 - expected property value
     * @param expected2 - expected response
     */
    @ParameterizedTest
    @CsvSource({
    	"account, 9999, first_name, Tomasz, Tomasz, OK",
    	"account, 9999, last_name, Kowalski, Kowalski, OK",
    })
    public void executeRequestTest2(String context,
                                    String accountNumber,
                                    String property,
                                    String value,
                                    String expected1,
                                    String expected2) {
        JSONObject jsonObject;
        JSONObject account;
        
        try {
            Map<String, String> params = new HashMap<String, String>();            
            params.put("number", accountNumber);           
            params.put("property", property);
            params.put("value", value);
            Request request = new PutRequest(context, params);
            String response = request.executeRequest();
            jsonObject = jsonFileHandler
                    .readJSONObject(accountsFilePath);
            account = (JSONObject)jsonObject.get(TestAccounts.TestAccount1.number);
            assertEquals(expected1, account.get(property));
            assertEquals(expected2, response);
        } catch (Exception e) {
            e.printStackTrace();
            fail(e);
        }
    }
    

    /**
     * Tests executeRequest method.
     * 
     * @param context - request context
     * @param senderAccountNumber - sender account number
     * @param receiverAccountNumber - receiver account number
     * @param transferAmount - transfer amount
     * @param expectedSenderBalance - expected sender balance
     * @param expectedReceiverBalance - expected receiver balance
     * @param expectedResponse - expected response
     */
    @ParameterizedTest
    @CsvSource({
    	"transfer, 9999, 8888, 1234.56, 8765.43, 10123.44, OK",
    	"transfer, 9999, 8888, 1.0, 9998.99, 8889.88, OK"
    })
    public void executeRequestTest3(String context,
    								String senderAccountNumber,
    								String receiverAccountNumber,
    								String transferAmount,
    								String expectedSenderBalance,
    								String expectedReceiverBalance,
    								String expectedResponse) {
        try {
            Map<String, String> params = new HashMap<String, String>();            
            params.put("sender", senderAccountNumber);           
            params.put("receiver", receiverAccountNumber);
            params.put("amount", transferAmount);
            Request request = new PutRequest(context, params);
            String response = request.executeRequest();
            
            JSONObject jsonObject = jsonFileHandler
                    .readJSONObject(accountsFilePath);
            JSONObject senderAccount = (JSONObject)jsonObject.get(senderAccountNumber);
            JSONObject receiverAccount = (JSONObject)jsonObject.get(receiverAccountNumber);
            
            assertEquals(expectedSenderBalance,
                    senderAccount.get("balance").toString());
            assertEquals(expectedReceiverBalance,
                    receiverAccount.get("balance").toString());
            assertEquals(expectedResponse, response);
        } catch (Exception e) {
            e.printStackTrace();
            fail(e);
        }
    }
}
