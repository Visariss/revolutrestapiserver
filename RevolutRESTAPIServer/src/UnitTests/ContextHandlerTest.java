package UnitTests;

import static org.junit.jupiter.api.Assertions.*;

import java.io.OutputStream;
import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mockito;

import com.sun.net.httpserver.HttpExchange;

import JSONManager.JSONFileHandler;
import RequestManager.Request;
import RequestManager.RequestFactory;
import ServerManager.ContextHandler;

/**
 * Contains tests for ContextHandler class.
 *
 */
class ContextHandlerTest {
    private static final String requestConfigFilePath = "config/request.json";
    private static final JSONFileHandler jsonFileHandler =
            new JSONFileHandler();    

	/**
	 * Tests checkParametersValidity method.
	 * 
	 * @param method - request method
	 * @param context - request context
	 */
	@ParameterizedTest
    @CsvSource({
        "GET, account",
        "GET, accounts",
        "PUT, account",
        "PUT, transfer",
        "POST, account",
        "POST, account"
    })
	public void checkParametersValidityTest(String method,
									        String context) {
		try {
	        JSONObject jsonRequestConfig =
	                jsonFileHandler.readJSONObject(requestConfigFilePath);
	        JSONObject contextSetups =
	                (JSONObject) jsonRequestConfig.get(method);
	        JSONArray parameterSetup = (JSONArray) contextSetups.get(context);
	        
	        //This section has to be handled in legacy way as
	        //there is no option to parameterized json array.
	        List<String> parameterSetupList = new LinkedList<>();	        
	        for (Object param : parameterSetup) {
	        	parameterSetupList.add((String) param);
	        }
	        
	        Map<String, String> params = parameterSetupList
	        	.stream()
	        	.collect(Collectors.toMap(property -> property, property -> ""));
	        
	        RequestFactory requestFactory = new RequestFactory();
	        ContextHandler contextHandler = new ContextHandler(context,
	        												   requestFactory);
	        assertDoesNotThrow(() -> contextHandler.checkParametersValidity(method, params));
		} catch (Exception e) {
            e.printStackTrace();
            fail(e);
		}
	}
	
	/**
	 * Tests decodeQuery method.
	 * 
	 * @param query - query to be decoded
	 * @param param1 - parameter name
	 * @param expectedValue1 - expected value of param1
	 * @param param2 - parameter name
	 * @param expectedValue2 - expected value of param2
	 */
	@ParameterizedTest
	@CsvSource({
        "number=9999&first_name=John, number, 9999, first_name, John"
    })
	public void decodeQueryTest(String query,
								String param1,
								String expectedValue1,
								String param2,
								String expectedValue2) {
		ContextHandler contextHandler = new ContextHandler(null, null);
		Map<String, String> params = contextHandler.decodeQuery(query);
		assertEquals(expectedValue1, params.get(param1));
		assertEquals(expectedValue2, params.get(param2));
	}
	
	/**
	 * Tests handle method.
	 * 
	 * @param method - request method
	 * @param context - request context
	 * @param URIString - URI String with query parameters
	 * @param expectedStatusCode - expected status code
	 * @param expectedResponse - expected response
	 */
	@ParameterizedTest
	@CsvSource({
        "GET, doc, /doc, 200, OK",
        "GET, account, /account?number=9999, 200, OK",
        "GET, accounts, /accounts, 200, OK",
        "PUT, account, /account?number=9999&property=first_name&value=John, 200, OK",
        "PUT, transfer, /transfer?sender=9999&receiver=8888&amount=11.11, 200, OK",
        "POST, account, /account?number=9999&first_name=John&last_name=Smith&balance=9999.99, 200, OK",
        "DELETE, account, /account?number=9999, 200, OK"
    })
	public void handleTest(String method,
                           String context,
                           String URIString,
                           int expectedStatusCode,
                           String expectedResponse) {
	    try {
    		URI URI = new URI(URIString);
    		String query = URI.getQuery();
    		
    		OutputStream outputMocked = Mockito.mock(OutputStream.class);
    		Mockito.doNothing().when(outputMocked).write(expectedResponse.getBytes());
            Mockito.doNothing().when(outputMocked).close();
    		
    		HttpExchange httpRequestMocked = Mockito.mock(HttpExchange.class);
    		Mockito.when(httpRequestMocked.getRequestMethod()).thenReturn(method);
    		Mockito.when(httpRequestMocked.getRequestURI()).thenReturn(URI);
    		Mockito.doNothing().when(httpRequestMocked)
    		    .sendResponseHeaders(expectedStatusCode, expectedResponse.length());
    		Mockito.when(httpRequestMocked.getResponseBody()).thenReturn(outputMocked);
    		
    		Request requestMocked = Mockito.mock(Request.class);
    		Mockito.when(requestMocked.executeRequest()).thenReturn(expectedResponse);
    		
    		Map<String, String> params;
    		if (query != null) {
                params = Arrays
                    .asList(query.split("&"))
                    .stream()
                    .map(pair -> pair.split("="))
                    .collect(Collectors.toMap(pair -> pair[0], pair -> pair[1]));
            } else {
                params = new HashMap<String, String>();
            }
    		
    		RequestFactory requestFactoryMocked = Mockito.mock(RequestFactory.class);
    		Mockito.when(requestFactoryMocked.create(method, context, params)).thenReturn(requestMocked);
    		
    		ContextHandler contextHandler = new ContextHandler(context, requestFactoryMocked);
            
    		contextHandler.handle(httpRequestMocked);
    		
    		Mockito.verify(httpRequestMocked, Mockito.times(1)).getRequestMethod();
    		Mockito.verify(httpRequestMocked, Mockito.times(1)).getRequestURI();
            Mockito.verify(requestFactoryMocked, Mockito.times(1)).create(method, context, params);
            Mockito.verify(requestMocked, Mockito.times(1)).executeRequest();
            Mockito.verify(httpRequestMocked, Mockito.times(1)).sendResponseHeaders(expectedStatusCode, expectedResponse.length());
            Mockito.verify(httpRequestMocked, Mockito.times(1)).getResponseBody();
            Mockito.verify(outputMocked, Mockito.times(1)).write(expectedResponse.getBytes());
            Mockito.verify(outputMocked, Mockito.times(1)).close();
	    } catch (Exception e) {
            e.printStackTrace();
            fail(e);
        }
	}

}
