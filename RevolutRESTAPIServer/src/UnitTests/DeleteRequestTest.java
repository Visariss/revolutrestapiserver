package UnitTests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;

import org.json.simple.JSONObject;

import AccountsManager.AccountsHandler;
import JSONManager.JSONFileHandler;
import RequestManager.DeleteRequest;
import RequestManager.Request;

/**
 * Contains tests for DeleteRequest class.
 *
 */
public class DeleteRequestTest {
    private final String accountsFilePath = "db/accounts.json";
    private final AccountsHandler accountsHandler =
            new AccountsHandler(accountsFilePath);
    private final JSONFileHandler jsonFileHandler =
            new JSONFileHandler();
    
    /**
     * Adds by predefined test account before each test.
     */
    @BeforeEach
    public void initialize() {
        accountsHandler.addAccount(TestAccounts.TestAccount1.number,
                                   TestAccounts.TestAccount1.firstName,
                                   TestAccounts.TestAccount1.lastName,
                                   TestAccounts.TestAccount1.balance);
        
        accountsHandler.addAccount(TestAccounts.TestAccount2.number,
					               TestAccounts.TestAccount2.firstName,
					               TestAccounts.TestAccount2.lastName,
					               TestAccounts.TestAccount2.balance);
    }
    
    /**
     * Deletes predefined test account after each test
     * if it is still present in accounts data store.
     */
    @AfterEach
    public void cleanUp() {
        if (accountsHandler.accountExists(TestAccounts.TestAccount1.number))
            accountsHandler.deleteAccount(TestAccounts.TestAccount1.number);
        
        if (accountsHandler.accountExists(TestAccounts.TestAccount2.number))
            accountsHandler.deleteAccount(TestAccounts.TestAccount2.number);
    }
    

    /**
     * Tests executeRequestTest method.
     * 
     * @param context - request context
     */
    @ParameterizedTest
    @CsvSource({"unknown"})
    public void executeRequestTest1(String context) {        
        try {            
            Request request = new DeleteRequest(context, null);
            assertThrows(Request.UnsupportedContextException.class,
                         () -> request.executeRequest());
        } catch (Exception e) {
            e.printStackTrace();
            fail(e);
        }
    }
    

    /**
     * Tests executeRequestTest method.
     * 
     * @param context - request context
     * @param accountNumber - account number
     * @param expectedResponse - expected response
     */
    @ParameterizedTest
    @CsvSource({"account, 9999, OK"})
    public void executeRequestTest2(String context,
                                    String accountNumber,
                                    String expectedResponse) {
        try {
            Map<String, String> params = new HashMap<String, String>();            
            params.put("number", accountNumber);
            
            Request request = new DeleteRequest(context, params);
            String response = request.executeRequest();
            JSONObject jsonObject = jsonFileHandler
                    .readJSONObject(accountsFilePath);
            JSONObject account = (JSONObject)jsonObject.get(TestAccounts.TestAccount1.number);
            assertNull(account);
            assertEquals(expectedResponse, response);
        } catch (Exception e) {
            e.printStackTrace();
            fail(e);
        }
    }
}
