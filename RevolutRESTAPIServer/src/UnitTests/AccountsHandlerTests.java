package UnitTests;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.io.IOException;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import AccountsManager.AccountsHandler;
import JSONManager.JSONFileHandler;

/**
 * Contains tests for AccountHandler class.
 *
 */
public class AccountsHandlerTests {
    private final String accountsFilePath = "db/accounts.json";
    private final AccountsHandler accountsHandler =
            new AccountsHandler(accountsFilePath);
    private final JSONFileHandler jsonFileHandler =
            new JSONFileHandler();
    
    /**
     * Adds by predefined test account before each test.
     */
    @BeforeEach
    public void initialize() {
        accountsHandler.addAccount(TestAccounts.TestAccount1.number,
                                   TestAccounts.TestAccount1.firstName,
                                   TestAccounts.TestAccount1.lastName,
                                   TestAccounts.TestAccount1.balance);
        
        accountsHandler.addAccount(TestAccounts.TestAccount2.number,
					               TestAccounts.TestAccount2.firstName,
					               TestAccounts.TestAccount2.lastName,
					               TestAccounts.TestAccount2.balance);
    }
    
    /**
     * Deletes predefined test account after each test
     * if it is still present in accounts data store.
     */
    @AfterEach
    public void cleanUp() {
        if (accountsHandler.accountExists(TestAccounts.TestAccount1.number))
            accountsHandler.deleteAccount(TestAccounts.TestAccount1.number);
        
        if (accountsHandler.accountExists(TestAccounts.TestAccount2.number))
            accountsHandler.deleteAccount(TestAccounts.TestAccount2.number);
    }

    /**
     * Tests adding account using addAccount method.
     */
    @Test
    public void addAccountTest() {        
        try {
            JSONObject jsonObject = jsonFileHandler
                    .readJSONObject(accountsFilePath);
            JSONObject account = (JSONObject)jsonObject
                    .get(TestAccounts.TestAccount1.number);
            
            assertNotNull(account);
            assertEquals(account.get("first_name"), TestAccounts.TestAccount1.firstName);
            assertEquals(account.get("last_name"), TestAccounts.TestAccount1.lastName);
            assertEquals(account.get("balance"), TestAccounts.TestAccount1.balance);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Tests getting account with given number using getAccount method.
     */
    @Test
    public void getAccountTest() {
        String response = accountsHandler.getAccount(TestAccounts.TestAccount1.number);
        
        try {
            JSONObject jsonObject = jsonFileHandler
                    .readJSONObject(accountsFilePath);
            JSONObject account = (JSONObject)jsonObject.get(TestAccounts.TestAccount1.number);
            assertEquals(account.toString(), response.toString());
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Tests getting all accounts as JSON String using getAccounts method.
     */
    @Test
    public void getAccountsTest() {
        String response = accountsHandler.getAccounts();
        
        try {
            JSONObject jsonObject = jsonFileHandler
                    .readJSONObject(accountsFilePath);
            assertEquals(jsonObject.toString(), response.toString());
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Tests editing account's field using editAccount method.
     *
     * @param property - property in JSON Object account
     * @param value - value of property
     * @param expectedResponse - expected response
     */
    @ParameterizedTest
    @CsvSource({
        "first_name, Tomasz, OK",
        "last_name, Nowak, OK",
        "balance, 1111.11, OK"
    })
    public void editAccountTest(String property,
                                String value,
                                String expectedResponse) {
        
        String response = accountsHandler
            .editAccount(TestAccounts.TestAccount1.number, property, value);
        
        try {
            JSONObject jsonObject = jsonFileHandler
                    .readJSONObject(accountsFilePath);
            JSONObject account = (JSONObject)jsonObject
                    .get(TestAccounts.TestAccount1.number);
            assertNotNull(account);
            assertEquals(value, account.get(property));
            assertEquals(expectedResponse, response);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Tests transfering between accounts using transfer method.
     * Note that sender is predefined from TestAccount class.
     * 
     * @param senderAccountNumber - sender account number
     * @param receiverAccountNumber - receiver account number
     * @param transferAmount - transfer amount
     * @param expectedSenderBalance - expected sender balance
     * @param expectedReceiverBalance - expected receiver balance
     * @param expectedResponse - expected response
     */
    @ParameterizedTest
    @CsvSource({
        "9999, 8888, 1234.56, 8765.43, 10123.44, OK",
        "9999, 8888, 1.0, 9998.99, 8889.88, OK"
    })
    public void transferTest(String senderAccountNumber,
                             String receiverAccountNumber,
                             String transferAmount,
                             String expectedSenderBalance,
                             String expectedReceiverBalance,
                             String expectedResponse) {
        String response = accountsHandler.transfer(senderAccountNumber,
                                                   receiverAccountNumber,
                                                   transferAmount);
        
        try {
            JSONObject jsonObject = jsonFileHandler
                    .readJSONObject(accountsFilePath);
            JSONObject senderAccount = (JSONObject)jsonObject
                    .get(TestAccounts.TestAccount1.number);
            JSONObject receiverAccount = (JSONObject)jsonObject
                    .get(TestAccounts.TestAccount2.number);
            assertNotNull(senderAccount);
            assertNotNull(receiverAccount);
            assertEquals(expectedSenderBalance,
                         senderAccount.get("balance").toString());
            assertEquals(expectedReceiverBalance,
                         receiverAccount.get("balance").toString());
            assertEquals(expectedResponse, response);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Tests deleting account using deleteAccount method.
     */
    @Test
    public void deleteAccountTest() {
        accountsHandler.deleteAccount(TestAccounts.TestAccount1.number);
        
        try {
            JSONObject jsonObject = jsonFileHandler
                    .readJSONObject(accountsFilePath);
            JSONObject account = (JSONObject)jsonObject
                    .get(TestAccounts.TestAccount1.number);
            assertNull(account);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
