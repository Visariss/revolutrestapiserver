package UnitTests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import JSONManager.JSONFileHandler;

/**
 * Contains tests for JSONFileHandler class.
 *
 */
public class JSONFileHandlerTests {
    private final String jsonFilePath = "db/test.json";
    private final JSONFileHandler jsonFileHandler =
            new JSONFileHandler();
    
    @BeforeEach
    public void initialize() throws Exception {
        FileWriter jsonFile = new FileWriter(jsonFilePath);
        JSONObject jsonObject = new JSONObject();
        jsonFile.write(jsonObject.toJSONString());
        jsonFile.flush();
        jsonFile.close();
    }
    
    
    @AfterEach
    public void cleanUp() {
        File file = new File(jsonFilePath);
        file.delete();
    }
    

    /**
     * Tests reading JSON file.
     */
    @Test
    public void readJSONObjectTest() {
        JSONObject jsonObject = null;
        try {
            jsonObject = jsonFileHandler.readJSONObject(jsonFilePath);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertNotNull(jsonObject);
    }

    /**
     * Tests writing to JSON file.
     */
    @Test
    public void writeJSONObjectTest() {
        String key = "key";
        String value = "value";
        JSONObject jsonObject = new JSONObject();
        
        // Generates unsolvable warning - JSONObject class, that extends
        // HashMap does not have type parameter <K,V> in class definition.
        jsonObject.put(key, value);
        try {
            jsonFileHandler.writeJSONObject(jsonFilePath, jsonObject);
        } catch (IOException e) {
            e.printStackTrace();
        }        

        try {
            jsonObject = jsonFileHandler.readJSONObject(jsonFilePath);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertEquals(jsonObject.get(key).toString(), value);
    }
}
