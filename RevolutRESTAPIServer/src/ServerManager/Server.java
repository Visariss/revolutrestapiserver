package ServerManager;
import java.io.IOException;

import java.util.Scanner;

import com.sun.net.httpserver.HttpServer;

import RequestManager.RequestFactory;

import java.net.InetSocketAddress;

/**
 * Main class.
 *
 */
public class Server {
    public static int port;
    
    private static HttpServer server;
    private static ContextHandler accountContextHandler;
    private static ContextHandler accountsContextHandler;
    private static ContextHandler transferContextHandler;
    private static ContextHandler docContextHandler;
    private static Scanner serverInput;
    
    
    /**
     * Builds server structure.
     * 
     * @throws IOException
     */
    private static void build() throws IOException {
        RequestFactory requestFactory = new RequestFactory();
        accountContextHandler = new ContextHandler("account", requestFactory);
        accountsContextHandler = new ContextHandler("accounts", requestFactory);
        transferContextHandler = new ContextHandler("transfer", requestFactory);
        docContextHandler = new ContextHandler("doc", requestFactory);
        
    	server = HttpServer.create(new InetSocketAddress(port), 0);
    	server.createContext("/account", accountContextHandler);
        server.createContext("/accounts", accountsContextHandler);
        server.createContext("/transfer", transferContextHandler);
        server.createContext("/doc", docContextHandler);
        server.setExecutor(null);
        
        serverInput = new Scanner(System.in);
    }
    
    
    /**
     * Closes server and release resources.
     * 
     * @throws IOException
     * @throws InterruptedException 
     */
    private static void close() throws IOException {
        serverInput.close();
    }
    
    
    /**
     * Listens keyboard input.
     */
    private static void listenInput() {
        while(true) {
            System.out.println(new StringBuilder().append("Server is running on port: ")
                                                  .append(port)
                                                  .append("\r\n")
                                                  .toString());
            System.out.println("Please type 'exit' to quit.\r\n");
            String input = serverInput.nextLine();

            if (input.trim().toLowerCase().equals("exit")) break;
        }
    }
    
    
    /**
     * Main method.
     * 
     * @param args - arguments
     */
    public static void main(String[] args) {
        port = Integer.parseInt(args[0]);
        
        try {
            build();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        server.start();
        listenInput();
        server.stop(0);
        
        try {
            close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}