package ServerManager;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import JSONManager.JSONFileHandler;
import RequestManager.Request;
import RequestManager.RequestFactory;

/**
 * Represents HTTP handler for different contexts.
 *
 */
public class ContextHandler implements HttpHandler {
    private static final String requestConfigFilePath = "config/request.json";
    private static final JSONFileHandler jsonFileHandler =
            new JSONFileHandler();
    private String context;
    private RequestFactory requestFactory;
    
    
    /**
     * Should be thrown if some parameters are invalid for
     * given request type and/or request context.
     *
     */
    private class InvalidParametersException extends Exception {
        private static final long serialVersionUID = 1324852372291257158L;

        public InvalidParametersException() {
            super("ERROR: Invalid parameters.\r\n");
        }
    }
    
    
    /**
     * Constructor.
     * 
     * @param context - request context
     * @param requestFactory - request factory
     */
    public ContextHandler(String context, RequestFactory requestFactory) {
    	this.context = context;
        this.requestFactory = requestFactory;
    }
    
    
    /**
     * Checks if parameters from request
     * query are the same as in configuration.
     * 
     * @param method - request method
     * @param params - request parameters
     * @throws InvalidParametersException
     * @throws ParseException
     * @throws IOException
     */
    public void checkParametersValidity(
            String method, Map<String, String> params)
                    throws InvalidParametersException, ParseException,
                           IOException {
        try {
            JSONObject jsonRequestConfig =
                    jsonFileHandler.readJSONObject(requestConfigFilePath);
            JSONObject contextSetups =
                    (JSONObject) jsonRequestConfig.get(method);
            JSONArray parameterSetup = (JSONArray) contextSetups.get(context);
            Set<String> givenParameters = params.keySet();
            System.out.println(method);
            System.out.println(params);
            System.out.println(parameterSetup);
            
            for (String param : params.keySet()) {
                if (!parameterSetup.contains(param))
                    throw new InvalidParametersException();
            }
            
            for (Object param : parameterSetup) {
                if (!givenParameters.contains((String) param))
                    throw new InvalidParametersException();
            }
        
        } catch (NullPointerException e) {
            e.printStackTrace();
            throw new InvalidParametersException();
        }
    }
    
    
    /**
     * Decodes query and returns parameters map.
     * 
     * @param query
     * @return - parameters map
     */
    public Map<String, String> decodeQuery(String query) {
        Map<String, String> params;
        
        if (query != null) {
            params = Arrays
                .asList(query.split("&"))
                .stream()
                .map(pair -> pair.split("="))
                .collect(Collectors.toMap(pair -> pair[0], pair -> pair[1]));
        } else {
            params = new HashMap<String, String>();
        }
        
        return params;
    }
    
    
    /* (non-Javadoc)
     * @see com.sun.net.httpserver
     * .HttpHandler#handle(com.sun.net.httpserver.HttpExchange)
     */
    @Override
    public void handle(HttpExchange httpRequest) throws IOException {
        String response;
        String method = httpRequest.getRequestMethod();
        String query = httpRequest.getRequestURI().getQuery();
        Map<String, String> params = decodeQuery(query);
        
        try {
            checkParametersValidity(method, params);
            Request request = requestFactory.create(method, context, params);
            response = request.executeRequest();
        } catch (Exception e) {
            e.printStackTrace();
            response = e.getMessage();
        }

        httpRequest.sendResponseHeaders(200, response.length());
        OutputStream output = httpRequest.getResponseBody();
        output.write(response.getBytes());            
        output.close();
    }
}
